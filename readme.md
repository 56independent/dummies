Adds 50 brand-new coloured nodes to the world with random colours.

Can be used for dynamic decoration and broken screens.