function random_hex_colour()
    local hex_color = "#"
    for i = 1, 6 do
        hex_color = hex_color .. string.format("%x", math.random(0, 15))
    end
    return hex_color
end

function defineNodeWithColourAndTexture(hexValue, texture, name, number)

    minetest.register_node("dummies:" .. name .. "_" .. tostring(number), {
        description = "Solid colour node" .. tostring(number),
        tiles = {texture .. ".png[colorize:" .. hexValue},
        groups = {cracky = 3, bakedclay = 1}
    })
end
for i = 1, 50 do
    local hex = random_hex_colour();

    defineNodeWithColourAndTexture(hex, "pure_white", "solid", i)
    defineNodeWithColourAndTexture(hex, "baked", "baked", i)
end
